package com.openrsc.server.net.rsc.struct;

import com.openrsc.server.net.rsc.enums.OpcodeIn;

public class ItemOnItemStruct extends AbstractStruct<OpcodeIn> {

	public int slotIndex1;
	public int slotIndex2;
}
