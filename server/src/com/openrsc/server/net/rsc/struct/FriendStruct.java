package com.openrsc.server.net.rsc.struct;

import com.openrsc.server.net.rsc.enums.OpcodeIn;

public class FriendStruct extends AbstractStruct<OpcodeIn> {

	public String player;
	public String message;
}
