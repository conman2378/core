package com.openrsc.server.net.rsc.struct;

import com.openrsc.server.net.rsc.enums.OpcodeIn;

public class SleepStruct extends AbstractStruct<OpcodeIn> {

	public int sleepDelay;
	public String sleepWord;

}
