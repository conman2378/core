package com.openrsc.server.net.rsc.struct;

import com.openrsc.server.net.rsc.enums.OpcodeIn;

public class CommandStruct extends AbstractStruct<OpcodeIn> {

	public String command;
}
