package com.openrsc.server.net.rsc.struct;

import com.openrsc.server.net.rsc.enums.OpcodeIn;

public class ItemOnMobStruct extends AbstractStruct<OpcodeIn> {

	public int serverIndex;
	public int slotIndex;
}
