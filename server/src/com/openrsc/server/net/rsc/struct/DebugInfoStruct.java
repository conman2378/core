package com.openrsc.server.net.rsc.struct;

import com.openrsc.server.net.rsc.enums.OpcodeIn;

public class DebugInfoStruct extends AbstractStruct<OpcodeIn> {

	public String infoString;
}
